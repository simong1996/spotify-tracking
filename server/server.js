//Dependencies
const express = require("express");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const { PrismaClient } = require("@prisma/client");
const expressGraphQL = require("express-graphql");
const app = express();
const prisma = new PrismaClient();
require("dotenv").config();

//Files & Functions
const schema = require("./prisma/schema.js");
const { Login, Callback } = require("./callbacks/callbacks.js");
const { latestPlayed } = require("./calls/functions");

app.use(cors()).use(cookieParser());

//API Calls
//Redirect to spotify authorization
app.get("/login", Login);

//When authorization is done, this request is called
app.get("/callback", Callback);

app.use(
  "/",
  expressGraphQL({
    schema: schema,
    graphiql: true,
    context: { prisma },
  })
);

const init = () => {
  latestPlayed();
  //updateAcessToken();
};

app.listen(5556);
console.log("Listening on 5556");
init();
