const { objectType, stringArg, intArg } = require("nexus");

const Mutation = objectType({
  name: "Mutation",
  definition(t) {
    t.field("createUser", {
      type: "User",
      args: {
        email: stringArg(),
        refreshToken: stringArg(),
        accessToken: stringArg(),
        after: stringArg()
      },
      resolve: (_, { email, refreshToken, accessToken, after }, ctx) => {
        return ctx.prisma.user.create({
          data: {
            email,
            refreshToken,
            accessToken,
            after
          }
        });
      }
    });

    t.field("updateUser", {
      type: "User",
      args: {
        userid: stringArg(),
        accessToken: stringArg({ required: false }),
        refreshToken: stringArg({ required: false }),
        after: stringArg({ required: false })
      },
      resolve: (_, { userid, accessToken, refreshToken, after }, ctx) => {
        return ctx.prisma.user.update({
          data: {
            accessToken: accessToken,
            refreshToken: refreshToken,
            after: after
          },
          where: { id: userid }
        });
      }
    });

    t.field("addPlayed", {
      type: "Played",
      args: {
        userid: stringArg(),
        date: stringArg(),
        name: stringArg(),
        duration: intArg(),
        image: stringArg({ required: false }),
        artists: stringArg({ list: true }),
        artistimg: stringArg({ required: false, list: true }),
        genres: stringArg({ list: true })
      },
      resolve: async (
        _,
        { userid, date, name, duration, image, artists, artistimg, genres },
        ctx
      ) => {
        const ao = { connect: [], create: [] },
          go = { connect: [], create: [] };
        let sng = await ctx.prisma.song.findOne({ where: { name: name } });
        for (let i = 0; i < artists.length; i++) {
          let ats = await ctx.prisma.artist.findOne({
            where: { name: artists[i] }
          });
          ats
            ? ao.connect.push({ id: ats.id })
            : ao.create.push({
                name: artists[i],
                image: artistimg[i]
              });
        }
        for (let k = 0; k < genres.length; k++) {
          let gre = await ctx.prisma.genre.findOne({
            where: { name: genres[k] }
          });
          gre
            ? go.connect.push({ id: gre.id })
            : go.create.push({
                name: genres[k]
              });
        }
        return ctx.prisma.played.create({
          data: {
            date,
            user: {
              connect: { id: userid }
            },
            song: sng
              ? {
                  connect: { id: sng.id }
                }
              : {
                  create: {
                    name,
                    duration,
                    image,
                    artists: ao,
                    genres: go
                  }
                }
          }
        });
      }
    });
  }
});

module.exports = Mutation;
