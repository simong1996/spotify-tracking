const { objectType, stringArg } = require("nexus");

const Query = objectType({
  name: "Query",
  definition(t) {
    t.crud.user();

    t.crud.users({
      filtering: true,
      ordering: true,
    });

    t.crud.playeds({
      filtering: true,
      ordering: true,
    });

    t.crud.songs({
      filtering: true,
      ordering: true,
    });

    t.crud.genres({
      filtering: true,
      ordering: true,
    });

    t.crud.artists({
      filtering: true,
      ordering: true,
    });

    t.field("getRandomSong", {
      type: "ReducedSong",
      resolve: async (_, args, ctx) => {
        const songs = await ctx.prisma.song.findMany();
        return { ...songs[Math.floor(Math.random() * songs.length)] };
      },
    });
  },
});

module.exports = Query;
