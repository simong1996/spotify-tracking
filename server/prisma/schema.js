const { makeSchema, objectType, intArg } = require("nexus");
const { nexusPrismaPlugin } = require("nexus-prisma");
const Mutation = require("./mutation.js");
const Query = require("./query.js");

//Graphql Schema
const User = objectType({
  name: "User",
  definition(t) {
    t.model.id();
    t.model.createdAt();
    t.model.email();
    t.model.refreshToken();
    t.model.accessToken();
    t.model.after();
    t.model.playeds({
      pagination: false,
    });
  },
});

const Played = objectType({
  name: "Played",
  definition(t) {
    t.model.id();
    t.model.date();
    t.model.song();
    t.model.user();
  },
});

const Song = objectType({
  name: "Song",
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.duration();
    t.model.artists({
      pagination: false,
    });
    t.model.genres({
      pagination: false,
    });
    t.model.image();
    t.model.playeds({
      pagination: false,
      filtering: true,
    });
  },
});

const ReducedSong = objectType({
  name: "ReducedSong",
  definition(t) {
    t.string("id");
    t.string("name");
    t.int("duration");
    t.string("image");
  },
});

const Artist = objectType({
  name: "Artist",
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.songs({
      pagination: false,
      filtering: true,
    });
    t.model.image();
  },
});

const Genre = objectType({
  name: "Genre",
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.songs({
      pagination: false,
      filtering: true,
    });
  },
});

const schema = makeSchema({
  types: [Query, Mutation, User, Played, Song, Genre, Artist, ReducedSong],
  plugins: [nexusPrismaPlugin()],
  outputs: {
    schema: __dirname + "/generated/schema.graphql",
    typegen: __dirname + "/generated/nexus.ts",
  },
});

module.exports = schema;
