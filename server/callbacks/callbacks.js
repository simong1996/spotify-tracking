//Dependencies
const querystring = require("querystring");

//Spotify API connection
const scope = "user-read-private user-read-email user-read-recently-played";
const stateKey = "spotify_auth_state";

//Files & Functions
const { loopSongs } = require("./../calls/functions");
const {
  checkingUsers,
  updatingUser,
  creatingUser,
} = require("./../calls/tracking");
const {
  gettingTokens,
  gettingUserInfo,
  gettingSongs,
} = require("./../calls/spotify");

//Generate a random string
const generateRandomString = (length) => {
  let text = "";
  const possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

const Login = (req, res) => {
  const state = generateRandomString(16);
  res.cookie(stateKey, state);
  console.log(stateKey, state);
  res.redirect(
    `https://accounts.spotify.com/authorize?${querystring.stringify({
      response_type: "code",
      client_id: process.env.CLIENT_ID,
      scope: scope,
      redirect_uri: process.env.DEV_REDIRECT_URI,
      state: state,
    })}`
  );
};

const Callback = async (req, res) => {
  const code = req.query.code || null;
  const state = req.query.state || null;
  const storedState = req.query.state ? req.cookies[stateKey] : null;
  //Cheks if the samer user that clicked login is requesting callback
  if (state === null || state !== storedState) {
    res.redirect(
      `/#${querystring.stringify({
        error: "state_mismatch",
      })}`
    );
  } else {
    res.clearCookie(stateKey);
    //Logging the user in or setting up the new user.
    let getTokens = await gettingTokens(code);
    let getUserInfo = await gettingUserInfo(getTokens.access_token);
    let checkUsers = await checkingUsers(getUserInfo.email);
    if (checkUsers) {
      //If user exists update with new access and refresh tokens
      console.log("User does exist...");
      let updateUser = await updatingUser({
        userId: checkUsers,
        refreshToken: getTokens.refresh_token,
        accessToken: getTokens.access_token,
      });
      let resParams = updateUser
        ? { id: updateUser }
        : { error: "Something_Went_Wrong" };
      console.log("User successfully logged in.");
      res.redirect(
        `${process.env.CLIENT_URI}/?${querystring.stringify(resParams)}`
      );
    } else {
      //if user doesn't exist create new user...
      console.log("User does not exist...");
      let getSongs = await gettingSongs(getTokens.access_token);
      let createUser = await creatingUser({
        email: getUserInfo.email,
        refreshToken: getTokens.refresh_token,
        accessToken: getTokens.access_token,
        after: getSongs.cursors.after,
      });
      //...and upload lastest songs...
      await loopSongs(getSongs, getTokens.access_token, createUser);
      console.log("Created new user, logging him in...");
      let resParams = createUser
        ? { id: createUser }
        : { error: "Something_Went_Wrong" };
      console.log("User successfully logged in.");
      res.redirect(
        `${process.env.CLIENT_URI}/?${querystring.stringify(resParams)}`
      );
    }
  }
};

module.exports = { Login, Callback };
