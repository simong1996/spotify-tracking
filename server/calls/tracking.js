//Dependencies
const { createApolloFetch } = require("apollo-fetch");

//Creating fetch for graphql endpoint
const fetch = createApolloFetch({
  uri: process.env.GRAPHQL_URI,
});

//Checking if the user exists
const checkingUsers = async (email) => {
  console.log("Checking if the user exists...");
  let id;
  await fetch({
    query: `query UserInfo($where: UserWhereUniqueInput!) {
      user(where: $where) {
        id
      }
    }`,
    variables: {
      where: {
        email: email,
      },
    },
  })
    .then((res) => {
      id = res.data ? res.data.user.id : null;
    })
    .catch((err) => {
      console.log(err);
    });
  return id;
};

//Updating user with new access, refresh and/or after token
const updatingUser = async (obj) => {
  console.log("Updating user with new access, refresh and/or after token...");
  let id;
  await fetch({
    query: `mutation UpdatingUser(
      $userId: String!
      ${obj.refreshToken ? "$refreshToken: String!" : ""}
      ${obj.accessToken ? "$accessToken: String!" : ""}
      ${obj.after ? "$after: String!" : ""}
    ) {
      updateUser(
        userid: $userId
        ${obj.accessToken ? "accessToken: $accessToken" : ""}
        ${obj.refreshToken ? "refreshToken: $refreshToken" : ""}
        ${obj.after ? "after: $after" : ""}
      ) {
        id
      }
    }`,
    variables: obj,
  }).then((res) => {
    id = res.data ? res.data.updateUser.id : null;
  });
  return id;
};

//Creating a new user
const creatingUser = async (obj) => {
  console.log("Creating a new user...");
  let id;
  await fetch({
    query: `mutation CreatingUser(
      $email: String!
      $refreshToken: String!
      $accessToken: String!
      $after: String!
    ) {
      createUser(
        email: $email
        refreshToken: $refreshToken
        accessToken: $accessToken
        after: $after
      ) {
        id
      }
    }`,
    variables: obj,
  }).then((res) => {
    id = res.data ? res.data.createUser.id : null;
  });
  return id;
};

//Adding each played item to the database
const addingPlayed = async (obj) => {
  console.log("Adding this played song to the db...");
  await fetch({
    query: `mutation AddPlayed(
      $userid: String!
      $date: String!
      $name: String!
      $duration: Int!
      $image: String!
      $artists: [String!]!
      $artistimg: [String!]!
      $genres: [String!]!
    ) {
      addPlayed(
        userid: $userid
        date: $date
        name: $name
        duration: $duration
        image: $image
        artists: $artists
        artistimg: $artistimg
        genres: $genres
      ) {
        id
      }
    }`,
    variables: obj,
  });
};

//Getting all the users
const gettingUsers = async (_) => {
  console.log("Getting all the users...");
  let ids;
  await fetch({
    query: `query users{
      users {
        id
        refreshToken
        accessToken
        after
      }  
    }`,
  }).then((res) => {
    ids = res.data ? res.data.users : null;
  });
  return ids;
};

module.exports = {
  gettingUsers,
  addingPlayed,
  creatingUser,
  updatingUser,
  checkingUsers,
};
