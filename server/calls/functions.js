const { gettingArtists, gettingTokens, gettingSongs } = require("./spotify");
const { addingPlayed, gettingUsers, updatingUser } = require("./tracking");

const loopSongs = async (songObj, atoken, userid) => {
  for (let i = 0; i < songObj.items.length; i++) {
    let getArtists = await gettingArtists(
      songObj.items[i].track.artists,
      atoken
    );
    const genresArr = []
      .concat(...getArtists.artists.map(artist => artist.genres))
      .filter((item, i, self) => self.indexOf(item) === i);
    const artistNameArr = getArtists.artists.map(artist => artist.name);
    const artistImgArr = getArtists.artists.map(artist =>
      artist.images.length ? artist.images[0].url : "Default Image"
    );
    await addingPlayed({
      userid: userid,
      date: songObj.items[i].played_at,
      name: songObj.items[i].track.name,
      duration: songObj.items[i].track.duration_ms,
      image: songObj.items[i].track.album.images.length
        ? songObj.items[i].track.album.images[0].url
        : artistImgArr[0],
      artists: artistNameArr,
      artistimg: artistImgArr,
      genres: genresArr
    });
  }
};

//Loops through all the users and updates their access token, every 53min
// const updateAcessToken = _ => {
//   setInterval(async _ => {
//     let getUsers = await gettingUsers();
//     for (let i = 0; i < getUsers.length; i++) {
//       let getAToken = await gettingTokens(false, getUsers[i].refreshToken);
//       await updatingUser({
//         userId: getUsers[i].id,
//         accessToken: getAToken.access_token
//       });
//     }
//   }, 3180000);
// };
// 3180000
//Loops through all the users and gets the latest 50 songs after the after-token
//Updating db with new played and new after token. This will happen every 3h
const latestPlayed = _ => {
  setInterval(async _ => {
    let getUsers = await gettingUsers();
    for (let i = 0; i < getUsers.length; i++) {
      let getAToken = await gettingTokens(false, getUsers[i].refreshToken);
      let getSongs = await gettingSongs(
        getAToken.access_token,
        getUsers[i].after
      );
      if (getSongs.items.length) {
        await loopSongs(getSongs, getAToken.access_token, getUsers[i].id);
        await updatingUser({
          userId: getUsers[i].id,
          after: getSongs.cursors.after,
          acessToken: getAToken.access_token
        });
      }
    }
  }, 3180000);
};
//10800000
module.exports = {
  latestPlayed,
  //updateAcessToken,
  loopSongs
};
