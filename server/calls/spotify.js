//Dependencies
const reqpromise = require("request-promise");

//Getting tokens based on what we get from the spotify.
const gettingTokens = (code, rtoken = null) => {
  console.log("Getting tokens from user...");
  let form = code
    ? {
        code: code,
        redirect_uri: process.env.DEV_REDIRECT_URI,
        grant_type: "authorization_code"
      }
    : {
        grant_type: "refresh_token",
        refresh_token: rtoken
      };
  return reqpromise({
    url: "https://accounts.spotify.com/api/token",
    method: "POST",
    form: form,
    headers: {
      Authorization: `Basic ${new Buffer(
        `${process.env.CLIENT_ID}:${process.env.CLIENT_SECRET}`
      ).toString("base64")}`
    },
    json: true
  });
};

//Getting user information with the access token
const gettingUserInfo = atoken => {
  console.log("Getting user information...");
  return reqpromise({
    url: "https://api.spotify.com/v1/me",
    method: "GET",
    headers: { Authorization: `Bearer ${atoken}` },
    json: true
  });
};

//Get 50 most recent played songs
const gettingSongs = (atoken, after = null) => {
  console.log("Getting the 50 most recent played songs...");
  return reqpromise({
    url: `https://api.spotify.com/v1/me/player/recently-played?limit=50${
      after ? `&after=${after}` : ""
    }`,
    headers: { Authorization: `Bearer ${atoken}` },
    method: "GET",
    json: true
  });
};

//Getting all artists for a song
const gettingArtists = (artists, atoken) => {
  console.log("Getting artist info for this song...");
  let artistsIdString = "";
  for (let i = 0; i < artists.length; i++) {
    artistsIdString += `${artists[i].id},`;
  }
  return reqpromise({
    url: `https://api.spotify.com/v1/artists?ids=${artistsIdString.slice(
      0,
      -1
    )}`,
    method: "GET",
    headers: { Authorization: `Bearer ${atoken}` },
    json: true
  });
};

module.exports = {
  gettingArtists,
  gettingSongs,
  gettingTokens,
  gettingUserInfo
};
